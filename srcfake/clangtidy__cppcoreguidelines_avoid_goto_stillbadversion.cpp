/** 
 https://releases.llvm.org/12.0.0/tools/clang/tools/extra/docs/clang-tidy/checks/cppcoreguidelines-avoid-goto.html

 The usage of goto for control flow is error prone and should be replaced with looping constructs. Only forward jumps in nested loops are accepted.
 This check implements ES.76 from the CppCoreGuidelines and 6.3.1 from High Integrity C++.
 For more information on why to avoid programming with goto you can read the famous paper A Case against the GO TO Statement..
 The check diagnoses goto for backward jumps in every language mode. These should be replaced with C/C++ looping constructs.
*/
#include <iostream>

int main13(){
	int i = 0;
		
	if (i < 100) {
	  ++i;
	  std::cout << "do dummy" << std::endl; //execute here
	}
	return 0;
}
