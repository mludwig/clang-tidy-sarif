#!/bin/bash
# analyze code of this project with clang, produce a sarif

# ./cs9_build.sh
# creates:
# ./build/compile_commands.json
# plus all the ./build

echo "===executing script $0 START==="



# clang-tidy rules, must be close to the sources and can stay in the ${CI_PROJECT_DIR}
#cp -v ${CI_PROJECT_DIR}/.clang-tidy ${CACHE_DIR}/src
#cp -v ${CI_PROJECT_DIR}/.clang-format ${CACHE_DIR}/src
     
# various checks

find ./ -name "compile_commands.json" -exec ls -l \;


# source scope is inside ./build
PATTERN="c cpp cc C CC CPP h hpp H HPP C++ C++ h++ H++ cxx CXX hxx HXX"
QADIR=/home/mludwig/projects/clang-tidy-sarif/build
echo "all files in ${QADIR} "
ls -l ${QADIR}

# all sources and files are inside QADIR with their build structure. Lets do the analysis inside QADIR
cd ${QADIR}
FLIST=${QADIR}/filelist
for p in $PATTERN; do
    find ./ -name "*.${p}" | grep -v "metadata" >> ${FLIST}
done
echo "sources filelist= "
cat ${FLIST}

# run code analysis, using the compile_commands.json in the project dir as well
# parallelize this into a couple of tasks: this creates files x000000.split .. x000048.split
# a primitive but efficient way to use multi-core runners ;-)
NJOBS_MIN=10
NJOBS_MAX_NEW=30
rm -rf x*.split
split -d -a 6 --additional-suffix=.split -l ${NJOBS_MAX_NEW} ${FLIST}
ls -l x*.split
for fn0 in `ls x*.split`; do

    echo "parallelize using split file ${fn0}: "
    cat ${fn0}
    njobs=`ps | grep "clang-tidy" | wc | awk '{printf "%d\n", $1}'`
    echo "0number of clang-tidy jobs= ${njobs}"

    while [ ${njobs} -gt ${NJOBS_MIN} ]; do
        sleep 3
        njobs=`ps | grep "clang-tidy" | wc | awk '{printf "%d\n", $1}' `
        echo "1number of clang-tidy jobs= ${njobs}"
    done
    for fn in `cat ${fn0}`; do
        fname=`basename ${fn}`
        dname=`dirname ${fn}`
        echo "===analysing file ${fn} "
        clang-tidy --header-filter=".*" ${fn} > ${QADIR}/${fname}.ctr &
    done
       
done

# wait for last job to finish
njobs=`ps | grep "clang-tidy" | wc | awk '{printf "%d\n", $1}'`
echo "3number of clang-tidy jobs= ${njobs}"

while [ ${njobs} -gt 0 ]; do
   sleep 3
   njobs=`ps | grep "clang-tidy" | wc | awk '{printf "%d\n", $1}' `
   echo "4number of clang-tidy jobs= ${njobs}"
done
   
       
# we read the whole directory for ctr files with own c++ converter, outputting to output.sarif
# can't parallelize this because of file I/O aggregation
# this sarif output can become several 100MBytes easily
clang-tidy-sarif -i ${QADIR} -o ${QADIR}/output.sarif
ls -l ${QADIR}/output.sarif

## cleaning up
#for fn in `cat ${FLIST}`; do
#	 rm -fv clang-tidy-report/${fn}.ctr
#done


