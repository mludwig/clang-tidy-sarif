/**
# LICENSE:
# Copyright (c) 2023, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Author: Michael.Ludwig@cern.ch
*/
/**
 * sar.h
 */

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>

namespace sar_ns {

/**
 * load json sarif elements and
 * provide serialisation methods
 *
 *
 */
class SAR {

public:
	SAR():
		_jsonBuffer(""),
		_index_run(0),
		_index_result(0),
		_index_location(0),
		_globalResultCounter(0)
	{};
	~SAR(){};

/**
 * structure of the semantical content, without the json formatting braces stuff
 */
	typedef struct {
		std::string uri;
	} ARTIFACT_LOCATION_t;

	typedef struct {
		unsigned int startColumn;
		unsigned int startLine;
	} REGION_t;

	typedef struct {
		ARTIFACT_LOCATION_t artifactLocation;
		REGION_t region;
	} PHYSICAL_LOCATION_t;

	typedef struct {
		PHYSICAL_LOCATION_t physicalLocation;
	} LOCATION_t;

	typedef struct {
		std::string text;
	} MESSAGE_t;

	typedef struct {
		std::string ruleId;
		std::string level;
		std::vector <LOCATION_t> locations_vect;
		MESSAGE_t message;
	} RESULT_t;


	typedef struct {
		std::string informationUri;
		std::string name;
		std::string version;
	} DRIVER_t;

	typedef struct {
		DRIVER_t driver;
	} TOOL_t;

	typedef struct {
		std::vector<RESULT_t> results_vect;
		TOOL_t tool;
	} RUN_t;

	typedef struct {
		std::string version;
		std::string schema;
		std::vector<RUN_t> run_vect; // theoretically several runs, but most of the time only one
	} CONTENT_t;

	void add_run();
	void add_result(RUN_t *run );
	void add_location( RESULT_t *result );

	RUN_t* get_run( unsigned int i );
	RESULT_t* get_result(RUN_t *run, unsigned k );
	LOCATION_t* get_location(RESULT_t *result, unsigned k );

	unsigned int get_index_run(){ return(_index_run );} // last valid
	unsigned int get_index_result(){ return(_index_result );} // last valid
	unsigned int getGlobalResultCounter(){ return(_globalResultCounter); }

	void set_ruleId( RESULT_t *result, std::string rule );
	void set_level( RESULT_t *result, std::string level );

	void set_regionLine( LOCATION_t *location, unsigned int line );
	void set_regionColumn( LOCATION_t *location, unsigned int col );
	void set_artifactLocationUri( LOCATION_t *location, std::string uri );
	void set_messageText( RESULT_t *result, std::string msg );

	void set_toolInformationUri( RUN_t *run, std::string msg );
	void set_toolName( RUN_t *run, std::string msg );
	void set_toolVersion( RUN_t *run, std::string msg );

	void serialize2json( std::string filename );


private:
	CONTENT_t _content; // semantical structured content
	std::string _jsonBuffer; // end result in json
	unsigned int _index_run;
	unsigned int _index_result;
	unsigned int _index_location;
	unsigned int _globalResultCounter;
};

} // namespace sar_ns
