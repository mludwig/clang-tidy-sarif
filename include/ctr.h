/**
# LICENSE:
# Copyright (c) 2023, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Author: Michael.Ludwig@cern.ch
*/
/**
 * ctr.h
 */


#include <string>
#include <iostream>
#include <fstream>
#include <regex>

namespace ctr_ns {

const std::string NORULE="no rule found";

/**
 * read and dissect oe clang-tidy stdout trace CTR
 * provide serialisation methods
 */
class CTR {

public:
	CTR( std::string fs ){
		_filestream = fs;
	};
	~CTR(){};

	int read();
	std::string get_rule();
	std::string get_level();
	unsigned int get_regionLine();
	unsigned int get_regionColumn();
	std::string get_artifactLocationUri();
	std::string get_messageText();
	std::string get_toolInformationUri();
	std::string get_toolName();
	std::string get_toolVersion();

	void removeFirstIssue();

private:
	std::string _filestream;
	std::string _rawBuffer;

};


} // ctr_ns
