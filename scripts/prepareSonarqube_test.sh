#!/bin/bash
# prepareSonarqube_test.sh
#
# local run on cs9, sonar9.9LTS, scanner5.0.1, goes into the project and is called by .gitlab-ci.yml
#
# produce a project specific sonar-project.properties file in the top level of the src
# sonar scanner gets called from the SRCTREE
# i.e. https://readthedocs.web.cern.ch/display/ICKB/sonarQube+sonar-project.properties
#

echo "===executing script $0 START 0==="
pwd
#
#
SONAR_SRC=src  
SONAR_PRJ_NAME=clang-tidy-sarif
SONAR_PRJ_VERSION=devel1.0
SONAR_WEBSERVER="https://sonarqube-sonarqube-test-accsoft-sonar.app.cern.ch"
#
# all the reports from the various tools, and the sarifs, go in here
REPORT_DIR=reports
ls -l ${REPORT_DIR}

# this is where the external reports are picked up from the community edition sonar-cxx plugin
SONAR_SCANNER_ROOT=/home/mladmin/Downloads/sonar-scanner-5.0.1.3006-linux
SONAR_PRJ_FILE="sonar-project.properties"
SONAR_SCANNER_FILE="sonar-scanner.properties"
UNAME="BE-ICS-QA"
#
# create a new sonar project file from scratch for this specific project
# see also https://docs.sonarsource.com/sonarqube/latest/analyzing-source-code/analysis-parameters/
rm -f ${SONAR_PRJ_FILE}; touch ${SONAR_PRJ_FILE}
echo "sonar.projectKey="${SONAR_PRJ_NAME}                  >> ${SONAR_PRJ_FILE}
echo "sonar.projectName="${SONAR_PRJ_NAME}                 >> ${SONAR_PRJ_FILE}
echo "sonar.projectVersion="${SONAR_PRJ_VERSION}      >> ${SONAR_PRJ_FILE}
echo "sonar.language=cxx"                             >> ${SONAR_PRJ_FILE}
echo "sonar.sources="${SONAR_SRC}                     >> ${SONAR_PRJ_FILE}
echo "sonar.sarifPaths="${REPORT_DIR}/output.sarif   >> ${SONAR_PRJ_FILE}
#
# see https://github.com/SonarOpenCommunity/sonar-cxx/wiki/CXX-Analysis-Parameters
echo "sonar.cxx.clangtidy.encoding=UTF8"              >> ${SONAR_PRJ_FILE}
echo "sonar.cxx.clangtidy.reportPaths="${REPORT_DIR} >> ${SONAR_PRJ_FILE}
echo "sonar.cxx.file.suffixes=.cxx,.cpp,.cc,.c,.hxx,.hpp,.hh,.h" >> ${SONAR_PRJ_FILE}
echo "sonar.cxx.errorRecoveryEnabled=True" >> ${SONAR_PRJ_FILE}
echo "sonar.cxx.includeDirectories=include" >> ${SONAR_PRJ_FILE}
echo "sonar.cxx.jsonCompilationDatabase=compile_commands.json"  >> ${SONAR_PRJ_FILE}

#echo "sonar.cxx.clangtidy.customRules= optional"     >> ${SONAR_PRJ_FILE}
#echo "sonar.cxx.clangtidy.charset=     optional"     >> ${SONAR_PRJ_FILE}
#echo "sonar.cxx.cppcheck.reportPaths="${REPORT_DIR}   >> ${SONAR_PRJ_FILE}
#echo "sonar.externalIssuesReportPaths="${REPORT_DIR}   >> ${SONAR_PRJ_FILE}
#echo "sonar.cxx.bullseye.reportPaths="${REPORT_DIR}  >> ${SONAR_PRJ_FILE}
#echo "sonar.cxx.clangsa.reportPaths="${REPORT_DIR} >> ${SONAR_PRJ_FILE}
#
# see external SARIF reports https://docs.sonarsource.com/sonarqube/latest/analyzing-source-code/importing-external-issues/importing-issues-from-sarif-reports/
echo "===sonarQube sonar-project.properties in file "${SONAR_PRJ_FILE}"==="
cat  ${SONAR_PRJ_FILE}
echo "=================================="
#
# create a sonar-scanner.properies on the fly for this project
# 
# - sonarqube9.9LTS works with with scanner5.0.1
# - we have the location of the web server handled dynamically per project. 
# - this config lives in ${SONAR_SCANNER_ROOT}/conf (docker) system-wide
# - if a web server is specified, use it, otherwise take default. 
rm -f ${SONAR_SCANNER_FILE}; touch ${SONAR_SCANNER_FILE}
echo "sonar.host.url=${SONAR_WEBSERVER}" >> ${SONAR_SCANNER_FILE}
echo "sonar.sourceEncoding=UTF-8" >> ${SONAR_SCANNER_FILE}
echo "===sonarQube sonar-scanner.properties in file ${SONAR_SCANNER_FILE}==="
cat  ${SONAR_SCANNER_FILE}
echo "=================================="

# copy the scanner config to the scanner conf directory
# the sonar-project.properties stays at the project top level dir
mkdir -p ${SONAR_SCANNER_ROOT}/conf
cp -v ${SONAR_SCANNER_FILE} ${SONAR_SCANNER_ROOT}/conf


# check if we see the sources
echo "===check if we see the sources==="
ls -l ${SONAR_SRC}
echo "==="

echo "your results will be published into SONAR_WEBSERVER= ${SONAR_WEBSERVER}"
echo "===executing script $0 END==="

