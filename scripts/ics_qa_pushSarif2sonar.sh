# =================
# pushing to sonar9
# =================
# running inside a container from gitlab-registry.cern.ch/jcopfw/qa/ics-fd-qa:qa-1.3
# on a gitlab runner
#
echo "===executing script $0 START==="
SONAR_TOKEN="sqp_b6486c5fab206473081fb92ed7a8fb218dd97f06"
SONAR_SCANNER_ROOT=/sonar-scanner-5.0.1.3006-linux/bin
SONAR_SCANNER=${SONAR_SCANNER_ROOT}/sonar-scanner

${SONAR_SCANNER} -X -Dsonar.token=${SONAR_TOKEN} | tee ./logs/sonar-scanner-trace.txt

echo "===executing script $0 END==="



