#!/bin/bash
#
# clang-tidy static code analysis 
#
CLANG_TIDY=/usr/bin/clang-tidy
CLANG_TIDY_SARIF=/usr/bin/clang-tidy-sarif
echo "===executing script $0 START==="
echo "project subdir which contains C++ source= $1"
echo "sonar project name (no whitespaces)= $2"
echo "sonar project version string (no whitespaces)= $3"
#
# we output a sonar-project.properties file which fits the project
if [ -z $1 ] 
then
	echo "please provide a directory with a C++ code tree"
	exit
fi
if [ -z $2 ] 
then
	echo "please provide a sonar project name (no whitespaces)"
	exit
fi
if [ -z $3 ] 
then
	echo "please provide a sonar project version string (no whitespaces)"
	exit
fi


# reports are relative to the SRCTREE
REPORT_DIR=$1/reports
mkdir -p ${REPORT_DIR}
echo "REPORT_DIR="${REPORT_DIR}

# check compile_commands.json from the build artifact, and copy it into the top level source dir so that clang-tidy finds it
echo "---looking for a compilation database---"
cd $1
pwd
find ./ -name "compile_commands.json" -exec cp -v {} $1 \;
echo "---"
#
# source scope: all files are explicitly listed in a filelist
echo "===looking for cpp/h source files, making a list from: "$1"==="
FLIST=${REPORT_DIR}/filelist
rm -rf ${FLIST}; touch ${FLIST}
PATTERN="c cpp cc C CC CPP h hpp H HPP C++ C++ h++ H++ cxx CXX hxx HXX"
for p in $PATTERN; do
	find $1 -name "*.${p}" | grep -v "metadata" >> ${FLIST}
done
echo "wrote list of src files into  ${FLIST}"
echo "===source scope is in filelist "${FLIST}" ==="
cat ${FLIST}
echo "================================="


# clang-tidy, using .clang-tidy configuration, /usr/local/bin as installed
#clang-tidy --version
#echo "===clang tidy configuration start==="
#clang-tidy -dump-config
#echo "===clang tidy configuration end==="
#echo "===clang tidy active rules start==="
#clang-tidy -list-checks
#echo "===clang tidy active rules end==="
# this gives all possible checks
#clang-tidy -checks='*' --list-checks

echo "===scanning files from list==="

echo ${FLIST}
ls -l ${FLIST}
echo "=============================="
for fn in `cat ${FLIST}`; do
    fname=`basename ${fn}`
    dname=`dirname ${fn}`
 
    echo "===analysing file ${fn} "
    clang-tidy ${fn} > ${REPORT_DIR}/${fname}.ctr    
    echo "wrote ${REPORT_DIR}/${fname}.ctr: "
    ls -l ${REPORT_DIR}/${fname}.ctr    
done
echo "===here are the reports==="
ls -l ${REPORT_DIR}
echo "==="


# we read the whole directory with own c++ converter, outputting to output.sarif
clang-tidy-sarif -i ${REPORT_DIR} -o ${REPORT_DIR}/output.sarif
echo "===here is the sarif report==="
ls -l ${REPORT_DIR}/*.sarif
echo "==="

## cleaning up
#for fn in `cat ${FLIST}`; do
#	 rm -fv ${REPORT_DIR}/${fname}.ctr
#done

echo "===executing script $0 END==="

