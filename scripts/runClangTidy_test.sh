#!/bin/bash
#
# clang-tidy static code analysis 
#
CLANG_TIDY=/usr/bin/clang-tidy
CLANG_TIDY_SARIF=/usr/bin/clang-tidy-sarif
echo "===executing script $0 START==="
echo "project subdir which contains C++ source= $1"

SRCTREE=.  # $1
#PNAME="qarulechecker"  # $2
#PVERSION="master"  # $3
CLANG_TIDY_SARIF=/home/mladmin/projects/clang-tidy-sarif-devel1.0/build/bin/clang-tidy-sarif



# reports are relative to the SRCTREE
REPORT_DIR=${SRCTREE}/reports
mkdir -p ${REPORT_DIR}
echo "REPORT_DIR="${REPORT_DIR}

# check compile_commands.json from the build artifact, and copy it into the top level source dir so that clang-tidy finds it
echo "---looking for a compilation database---"
cd ${SRCTREE}
pwd
find ./ -name "compile_commands.json" -exec cp -v {} $1 \;
echo "---"
#
# source scope: all files are explicitly listed in a filelist
echo "===looking for cpp/h source files, making a list from: ${SRCTREE} ==="
FLIST=${SRCTREE}/filelist
rm -rf ${FLIST}; touch ${FLIST}
PATTERN="c cpp cc C CC CPP h hpp H HPP C++ C++ h++ H++ cxx CXX hxx HXX"
for p in $PATTERN; do
	find ${SRCTREE} -name "*.${p}" | grep -v "metadata" >> ${FLIST}
	find ${SRCTREE} -name "*.${p}" | grep -v "metadata" 
done
echo "wrote list of src files into  ${FLIST}"
echo "===source scope is in filelist "${FLIST}" ==="
cat ${FLIST}
echo "================================="


# clang-tidy, using .clang-tidy configuration, /usr/local/bin as installed
#clang-tidy --version
#echo "===clang tidy configuration start==="
#clang-tidy -dump-config
#echo "===clang tidy configuration end==="
#echo "===clang tidy active rules start==="
#clang-tidy -list-checks
#echo "===clang tidy active rules end==="
# this gives all possible checks
#clang-tidy -checks='*' --list-checks

echo "===scanning files from list==="

echo ${FLIST}
ls -l ${FLIST}
echo "=============================="
for fn in `cat ${FLIST}`; do
    fname=`basename ${fn}`
    dname=`dirname ${fn}`
 
    echo "===analysing file ${fn} "
    clang-tidy ${fn} > ${REPORT_DIR}/${fname}.ctr    
    echo "wrote ${REPORT_DIR}/${fname}.ctr: "
    ls -l ${REPORT_DIR}/${fname}.ctr    
done


# we read the whole directory with own c++ converter, outputting to output.sarif
${CLANG_TIDY_SARIF} -i ${REPORT_DIR} -o ${REPORT_DIR}/output.sarif
echo "===here is the sarif report==="
ls -l ${REPORT_DIR}/*.sarif
echo "==="

## cleaning up
#for fn in `cat ${FLIST}`; do
#	 rm -fv ${REPORT_DIR}/${fname}.ctr
#done

echo "===executing script $0 END==="

