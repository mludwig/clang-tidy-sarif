#!/bin/bash
# version for ubuntu22.04 ml, sonar9.9LTS, scanner5.0.1, goes into the project and is called by .gitlab-ci.yml
# https://docs.sonarsource.com/sonarqube/latest/analyzing-source-code/scanners/sonarscanner/
#
# trigger the sonarQube scanner to pick up the reports
# fix certificate:
#
# 1 - Copy the certificate store <java_home>/jre/lib/security/cacerts
#     to a directory to import the sonar certificate only once
# 2 - Get the certificate from sonar server to a file (using openssl)
# 3 - Import the certificate into cacerts file with keytool
# 4 - Export the SONAR_SCANNER_OPTS variable containing the path to the certificate store 

echo "===executing script $0 START==="
cd ..


#SCANNER=$WDIR/sonar-scanner-2.7/bin/sonar-scanner
#SONAR_SCANNER_FILE=$WDIR/sonar-scanner-2.7/conf/sonar-scanner.properties
SONAR_SCANNER_ROOT=/sonar-scanner-5.0.1.3006-linux
SCANNER=${SONAR_SCANNER_ROOT}/bin/sonar-scanner
SONAR_SCANNER_PROP_FILE=${SONAR_SCANNER_ROOT}/conf/sonar-scanner.properties

echo "whoami="`whoami`
echo "hostname="`hostname`
echo "pwd="`pwd`
#echo "BASH_VERSION=${BASH_VERSION}"
CDIR=`pwd`
XXX=$(readlink -f `which java` | sed "s/java$//g")

# should be done by ics_fd_qa_prepareSonarqube.sh but lets see where they are
find / -name "sonar-scanner" -exec cat -b {} \;
find / -name "sonar-scanner.properties" -exec cat -b {} \;

# the java cert store location is different depending on the system install for java
# docker ubuntu
#cd $XXX/../../jre/lib/security
#
# ubuntu 2004.03
# /usr/lib/jvm/java-11-openjdk-amd64/lib/security/cacerts
cd $XXX/../../default-java/lib/security

echo "the java cert store is in "`pwd`
ls -al

cd ${CDIR}
# does not work for branches. HACK IT
SONAR_SERVER=` cat ${SONAR_SCANNER_PROP_FILE} | grep "sonar.host.url" | sed "s/=/ /g" | awk '{printf "%s", $2}' `
echo "sonar server/webpage is at= " ${SONAR_SERVER}




# https://sonarqube-sonarqube-test-accsoft-sonar.app.cern.ch
# user: test
# password : MjVB6XVA8W8JpqC
export SONAR_TOKEN="sqp_b6486c5fab206473081fb92ed7a8fb218dd97f06"
echo "using SONAR_TOKEN=${SONAR_TOKEN}"
echo "using SONAR_SERVER=${SONAR_SERVER}"

#SONAR_SERVER="https://cvl-sonarqube.cern.ch"
#echo "HARDCODED HACK DO NOT MERGE ... or maybe merge... sonar server/webpage is at= " ${SONAR_SERVER}


#mkdir -p ~/certificates
#cd ~/certificates
#
# get the certificate from the server and store it locally
#echo | openssl s_client -connect cvl-sonarqube.cern.ch:443 2>&1 | sed -n -e '/BEGIN\ CERTIFICATE/,/END\ CERTIFICATE/ p' > cvl-sonarqube.cern.ch.crt
#
# import the certificate into the cert store of java
#sudo keytool -import -file cvl-sonarqube.cern.ch.crt -alias sonar -keystore cacerts -storepass changeit
#
## slc6sboychen:9000 does not have a certificate
##echo | openssl s_client -connect slc6sboychen:9000 2>&1 | sed -n -e '/BEGIN\ CERTIFICATE/,/END\ CERTIFICATE/ p' > cvl-#sonarqube.ics-fd-qa.crt
##keytool -import -file cvl-sonarqube.ics-fd-qa.crt -alias sonar -keystore cacerts -storepass changeit
#export SONAR_SCANNER_OPTS="-Djavax.net.ssl.trustStore=~/certificates/cacerts"

#${SCANNER} -v
# -X for debugging, and no security
echo "calling ${SCANNER} -X -Dsonar.token=${SONAR_TOKEN}"
${SCANNER} -X -Dsonar.token=${SONAR_TOKEN}
# ${SCANNER}


echo "===executing script $0 END==="

