/**
# LICENSE:
# Copyright (c) 2023, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Author: Michael.Ludwig@cern.ch
*/
/**
 * sar.cpp
 */
#include <sar.h>


using namespace sar_ns;

/**
 * adds one run to content:run_vect
 * including one first item to the vectors
 * content:run_vect:results_vect
 * content:run_vect:results_vect:locations_vect
 */
void SAR::add_run(){
	RUN_t run;
	_content.run_vect.push_back( run );
	_index_run = _content.run_vect.size() - 1;
}

/**
 * returns the i-th run, usually i=0 for only one run
 */
SAR::RUN_t* SAR::get_run( unsigned int i ){
	if ( i < _content.run_vect.size() ){
		return &(_content.run_vect[ i ]);
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
				<< " run " << i << " does not exist" << std::endl;
		exit(-1);
	}
}

/**
 * adds one result to content:run_vect[]:results_vect[]
 */
void SAR::add_result(RUN_t *run ){
	RESULT_t result;
	run->results_vect.push_back( result );
	_index_result = run->results_vect.size() - 1;
	_globalResultCounter++;
	// std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " " << _index_result << std::endl;
}

/**
 * get the k-th result of a run, k>>0 usually
 */
SAR::RESULT_t* SAR::get_result(RUN_t *run, unsigned int k ){
	if ( k < run->results_vect.size() ){
		return &(run->results_vect[ k ]);
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
				<< " result " << k << " does not exist, size()= " << run->results_vect.size() << std::endl;
		exit(-1);
	}
}

/**
 * adds one location to content:run_vect[]:results_vect[]:locations_vect
 */
void SAR::add_location( RESULT_t *result ){
	LOCATION_t location;
	result->locations_vect.push_back( location );
	_index_location = result->locations_vect.size() - 1;
}

/**
 * the location of an issue is also the delimiter in the ctr file
 */
SAR::LOCATION_t* SAR::get_location(RESULT_t *result, unsigned int k ){
	if ( k < result->locations_vect.size() ){
		return &(result->locations_vect[ k ]);
	} else {
		std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
				<< " location " << k << " does not exist, size()= " << result->locations_vect.size() << std::endl;
		exit(-1);
	}
}

void SAR::set_ruleId( RESULT_t *result, std::string rule ){
	result->ruleId = rule;
}
void SAR::set_level( RESULT_t *result, std::string level ){
	result->level = level;
}
void SAR::set_regionLine( LOCATION_t *location, unsigned int line ){
	location->physicalLocation.region.startLine = line;
}
void SAR::set_regionColumn( LOCATION_t *location, unsigned int col ){
	location->physicalLocation.region.startColumn = col;
}
void SAR::set_messageText( RESULT_t *result, std::string msg ){
	result->message.text = msg;
}
void SAR::set_artifactLocationUri( LOCATION_t *location, std::string uri ){
	location->physicalLocation.artifactLocation.uri = uri;
}


void SAR::set_toolInformationUri( RUN_t *run, std::string ss ){
	run->tool.driver.informationUri = ss;
}
void SAR::set_toolName( RUN_t *run, std::string ss ){
	run->tool.driver.name = ss;
}
void SAR::set_toolVersion( RUN_t *run, std::string ss ){
	run->tool.driver.version = ss;
}


/**
 * at the end: take the semantic result structure and serialize it into the json.
 * This is a strictly hierarchical thing with 3 loops over the vects.
 * we dump the results into _jsonBuffer, all in the good order. Then we can write that to file.
 */
void SAR::serialize2json( std::string filename ){
	std::stringstream os;
	os << "{" << std::endl;
	os << "		\"version\": \"2.1.0\"," << std::endl;
	os << "		\"$schema\": \"http://json.schemastore.org/sarif-2.1.0\"," << std::endl;
	os << "		\"runs\": [" << std::endl;


	int run_counter = 0;
	int result_counter = 0;
	//std::cout << __FILE__ << " " << __LINE__ << " ** have " << _content.run_vect.size() << " runs" << std::endl;
	for (auto it_run = begin( _content.run_vect ); it_run != end( _content.run_vect ); ++it_run) {
		// std::cout << __FILE__ << " " << __LINE__ << " writing run " << run_counter << std::endl;

		// run start
		os << "			{" << std::endl;

		// results start
		os << "				\"results\" : [" << std::endl;

		//std::cout << __FILE__ << " " << __LINE__ << " *** have " << it_run->results_vect.size() << " results" << std::endl;
		for (auto it_result = begin( it_run->results_vect ); it_result != end( it_run->results_vect ); ++it_result) {
			// std::cout << __FILE__ << " " << __LINE__ << " writing result " << result_counter << std::endl;

			// ruleId: "ruleId": "fake-clang-tidy-mem-hole",
			if ( result_counter > 0 ) {
				os << "         	," << std::endl;
			}
			os << "				{" << std::endl;
			os << "					\"ruleId\": \"" << it_result->ruleId << "\"," << std::endl;

			// level :        "level": "error",
			os << "					\"level\": \"" << it_result->level << "\"," << std::endl;

			// can we have many locations for one result?
			int location_counter = 0;
			//std::cout << __FILE__ << " " << __LINE__ << " **** have " << it_result->locations_vect.size() << " locations" << std::endl;
			for (auto it_location = begin ( it_result->locations_vect ); it_location != end( it_result->locations_vect ); ++it_location) {
				//std::cout << __FILE__ << " " << __LINE__ << " writing location " << location_counter << std::endl;

		         /**"locations": [
		            {
		          */
				os << "					\"locations\": [" << std::endl;
				os << "						{" << std::endl;

		        	 /**
	             "physicalLocation": {
	                "artifactLocation": {
	                  "uri": "../venusOpcPilotServer-master/Device/src/DSCADAPILOT.cpp"
	                },
	                "region": {
	                  "startColumn": 10,
	                  "startLine": 23
	                }
	              }
				 */
				os << "							\"physicalLocation\": { " << std::endl;
				os << "								\"artifactLocation\": { "<< std::endl;
				os << "									\"uri\" : \"" << it_location->physicalLocation.artifactLocation.uri <<  "\"" << std::endl;
				os << "								}," << std::endl;
				os << "								\"region\" : {" << std::endl;
				os << "									\"startColumn\": " <<  it_location->physicalLocation.region.startColumn << "," << std::endl;
				os << "									\"startLine\": " <<  it_location->physicalLocation.region.startLine << "" << std::endl;
				os << "								}" << std::endl;
				os << "							}" << std::endl;
				os << "						}" << std::endl;
				os << "					]," << std::endl;

				location_counter++;
			}
			/**
	         * MESSAGE_t
	         *   "message": {
	            "text": "'boost/thread.hpp' file not found [clang-diagnostic-error] "
	          }
	        */
			os << "   				\"message\": {" << std::endl;
			os << "						\"text\": \"" << it_result->message.text << "\"" << std::endl;
			os << "					} " << std::endl;

			// this result end
			os << "				}" << std::endl;
			result_counter++;
		}

		// end results
		os << "				], " << std::endl;

		/**
	    // TOOL_t
	      "tool": {
	        "driver": {
	          "informationUri": "https://clang.llvm.org/extra/index.html",
	          "name": "clang-tidy",
	          "version": "14.0.0 Ubuntu LLVM version"
	        }
	      }
		 */
		os << "				\"tool\" : {" << std::endl;
		os << "					\"driver\" : {" << std::endl;
		os << "						\"informationUri\" : \"" << it_run->tool.driver.informationUri << "\"," << std::endl;
		os << "						\"name\" : \"" << it_run->tool.driver.name << "\"," << std::endl;
		os << "						\"version\" : \"" << it_run->tool.driver.version << "\"" << std::endl;
		os << "					}" << std::endl;
		os << "				}" << std::endl;

		// this run end
		os << "			}" << std::endl;

		// all runs end
		os << "		]" << std::endl;

		run_counter++;
	}
	// end all
	os << "} " << std::endl;

	_jsonBuffer = os.str();

	std::ofstream of;
	of.open( filename );
	of << _jsonBuffer;
	of.close();

	std::cout << "wrote " << result_counter << " results to file " << filename << std::endl;

}
