/**
# LICENSE:
# Copyright (c) 2023, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Author: Michael.Ludwig@cern.ch
*/
/**
 * ctr.cpp
 *
 * these methods just do primitive string pattern searches on the ctr buffer
 */
#include <ctr.h>


using namespace ctr_ns;

/**
 * opens file and does single char reading until the end into a std:string buffer
 * returns the nb chars read
 */

int CTR::read(){
	std::ifstream ifs( _filestream.c_str(), std::ios_base::in);
	_rawBuffer.assign( ( std::istreambuf_iterator<char>(ifs) ),
			(std::istreambuf_iterator<char>() ) );
	return( _rawBuffer.size() );
}



/**
 * extracting a rule from the buffer, e.g.
 *[clang-analyzer-core.NullDereference]
 */
std::string CTR::get_rule(){
	std::string rule = ctr_ns::NORULE;
	// std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " _rawBuffer= " << _rawBuffer << std::endl;
	std::size_t p0 = _rawBuffer.find_first_of("[");
	std::size_t p1 = _rawBuffer.find_first_of("]");
	if ( p1-p0 >= 4 ){ // minimum rule name is 3 chars
		rule = _rawBuffer.substr( p0+1, p1-p0-1 );
	}
	// std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " rule= " << rule << std::endl;

	return( rule );
}

/**
 * extracting a level from the buffer, e.g.
 * venusOpcPilotServer/Device/src/DGENERICPILOT.cpp:22:10: error:
 * clang-tidy just has just 3 levels possible: error, warning, note
 */
std::string CTR::get_level(){
	std::string rule = ctr_ns::NORULE;
	// std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " _rawBuffer= " << _rawBuffer << std::endl;
	if ( _rawBuffer.find(" error:") !=std::string::npos ) return("error");
	if ( _rawBuffer.find(" warning:") !=std::string::npos ) return("warning");
	if ( _rawBuffer.find(" note:") !=std::string::npos ) return("note");
	return( "unknown" );
}

/**
 * remove the first issue in the ctr rawBuffer so that we can iterate over all
 *
 * we identify an issue with the primary key: {artifactLocationUri, line, column}
 * the issues are delimited by this key.
 * we use this key to remove the corresponding lines from the _rawBuffer, by looking at the NEXT issue's key
 */
void CTR::removeFirstIssue(){
	// still unmod _rawBuffer
	//std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << std::endl
	//		<< "_rawBuffer= " << _rawBuffer << std::endl << std::endl;

	unsigned int issue_line0 = get_regionLine();
	unsigned int issue_column0 = get_regionColumn();
	std::string issue_uri0 = get_artifactLocationUri();
	//std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " key0 ingredients issue_line0= " << issue_line0 << std::endl;
	//std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " key0 ingredients issue_column0= " << issue_column0 << std::endl;
	//std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " key0 ingredients issue_uri0= " << issue_uri0 << std::endl;
	std::string key0 = "/"+issue_uri0 + ":" + std::to_string(issue_line0) +":" + std::to_string(issue_column0)+":" ;
	ssize_t pos0 = _rawBuffer.find( key0, 0 );
	// std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " key0 pos0= " << pos0 << std::endl;
	_rawBuffer.erase( 0, key0.length() ); // invalidate the issue: erase that line

	//std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " erased key0: " << std::endl
	//		<< " _rawBuffer= " << _rawBuffer << std::endl << std::endl;

	// look for the next issue's key
	unsigned int issue_line1 = get_regionLine();
	unsigned int issue_column1 = get_regionColumn();
	std::string issue_uri1 = get_artifactLocationUri();

	//std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " key1 ingredients issue_line1= " << issue_line1 << std::endl;
	//std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " key1 ingredients issue_column1= " << issue_column1 << std::endl;
	//std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " key1 ingredients issue_uri1= " << issue_uri1 << std::endl;
	std::string key1 = "/"+issue_uri1 + ":" + std::to_string(issue_line1) +":" + std::to_string(issue_column1)+":" ;

	ssize_t pos1 = _rawBuffer.find( key1 );
	// std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " key1 pos1= " << pos1 << std::endl;

	// cleanup any leftover crap between first issue and next issue...
	if ( pos1 != std::string::npos ){
		_rawBuffer.erase( 0, pos1 - 1 );
	} else {
		// ... or erase everything because we are past the last issue
		_rawBuffer.erase( _rawBuffer.begin(), _rawBuffer.end());
	}

	//std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " erased top issue/key, remaining buffer= " << std::endl
	//	<< " _rawBuffer= " << _rawBuffer << std::endl << std::endl;

}


/**
 * get line 84 from:
 *  /home/mludwig/projects/sarifPython/src/bugprone-dangling-handle.cpp:84:29:
 *
 * must fail:
 *   error: 'opcua_baseobjecttype.h' file not found with <angled> include; use "quotes" instead [clang-diagnostic-error]
 */
unsigned int CTR::get_regionLine(){
	unsigned int nline = 0;
	std::size_t p0, p1, p2, p3, p4;

	bool found = false;
	do {
		// search for regex :<line>:<column>:
		p0 = _rawBuffer.find(":");
		p1 = _rawBuffer.find_first_of("0123456789", p0);
		p2 = _rawBuffer.find(":", p1);
		p3 = _rawBuffer.find_first_of("0123456789", p2);
		p4 = _rawBuffer.find(":", p3);

		/**
		 *
		 std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
				<< " p0= " << p0
				<< " p1= " << p1
				<< " p2= " << p2
				<< " p3= " << p3
				<< " p4= " << p4 << std::endl;
		 */

		// all pos are found and the thing looks like a :<line>:<column>: sequence
		if ( (p0 + 1 == p1) && ( p2 + 1 == p3 ) && (p3 < p4 )) {
			if (( p0 != std::string::npos

					&& p1 != std::string::npos
					&& p2 != std::string::npos
					&& p3 != std::string::npos
					&& p3 != std::string::npos )){

				/** debug:
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ <<
						" p1-p0= " << _rawBuffer.substr( p0, p1-p0 ) <<
						" p2-p1= " << _rawBuffer.substr( p1, p2-p1 ) <<
						" p3-p2= " << _rawBuffer.substr( p2, p3-p2 ) <<
						" p4-p3= " << _rawBuffer.substr( p3, p4-p3 ) << std::endl;
				 */
				std::string sline = _rawBuffer.substr( p1, p2-p1 );
				try {
					std::size_t pos;
					nline = std::stoi( sline, &pos);
					found = true;
				} catch (...) {
					found = false;
					std::cout << __FILE__ << " " << __LINE__<< " std::invalid_argument::what(): can not convert this to a number: " << sline << std::endl;
					//return( 0 );
				}
			} else {
				// could not even find a : so there is certainly no key
				return( 0 );
			}
		} else {
			// we delete the first bit between p0 and p1 and try again
			if ( p0 < _rawBuffer.size() ){
				_rawBuffer.erase( p0, p1-p0);
			} else {
				return( 0 );
			}
		}
	} while ( !found );
	return( nline );
}

/**
 * get column 29 from:
 *  /home/mludwig/projects/sarifPython/src/bugprone-dangling-handle.cpp:84:29:
 *
 * must fail:
 *   error: 'opcua_baseobjecttype.h' file not found with <angled> include; use "quotes" instead [clang-diagnostic-error]
 */
unsigned int CTR::get_regionColumn(){
	unsigned int ncolumn = 0;
	std::size_t p0, p1, p2, p3, p4;

	bool found = false;
	do {
		// search for regex :<line>:<column>:
		p0 = _rawBuffer.find(":");
		p1 = _rawBuffer.find_first_of("0123456789", p0);
		p2 = _rawBuffer.find(":", p1);
		p3 = _rawBuffer.find_first_of("0123456789", p2);
		p4 = _rawBuffer.find(":", p3);

		/**
		 *
		 std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
				<< " p0= " << p0
				<< " p1= " << p1
				<< " p2= " << p2
				<< " p3= " << p3
				<< " p4= " << p4 << std::endl;
		 */

		// all pos are found and the thing looks like a :<line>:<column>: sequence
		if ( (p0 + 1 == p1) && ( p2 + 1 == p3 ) && (p3 < p4 )) {
			if (( p0 != std::string::npos

					&& p1 != std::string::npos
					&& p2 != std::string::npos
					&& p3 != std::string::npos
					&& p3 != std::string::npos )){

				/** debug:
				std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ <<
						" p1-p0= " << _rawBuffer.substr( p0, p1-p0 ) <<
						" p2-p1= " << _rawBuffer.substr( p1, p2-p1 ) <<
						" p3-p2= " << _rawBuffer.substr( p2, p3-p2 ) <<
						" p4-p3= " << _rawBuffer.substr( p3, p4-p3 ) << std::endl;
				 */
				std::string scolumn = _rawBuffer.substr( p3, p4-p3 );
				try {
					std::size_t pos;
					ncolumn = std::stoi( scolumn, &pos);
					found = true;
				} catch (...) {
					found = false;
					std::cout << __FILE__ << " " << __LINE__<< " std::invalid_argument::what(): can not convert this to a number: " << scolumn << std::endl;
					//return( 0 );
				}
			} else {
				// could not even find a : so there is certainly no key
				return( 0 );
			}
		} else {
			// we delete the first bit between p0 and p1 and try again
			if ( p0 < _rawBuffer.size() ){
				_rawBuffer.erase( p0, p1-p0);
			} else {
				return( 0 );
			}
		}
	} while ( !found );
	return( ncolumn );
}

/**
 * /home/mludwig/projects/sarifPython/src/bugprone-dangling-handle.cpp:81:28: warning: object backing the pointer will be destroyed at the end of the full-expression [clang-diagnostic-dangling-gsl]
 * just everything until the first :
 * drop the leading /
 *
 * must fail:
 *  error: 'opcua_baseobjecttype.h' file not found with <angled> include; use "quotes" instead [clang-diagnostic-error]
 *
 */
std::string CTR::get_artifactLocationUri(){
	std::string uri = "";
	// looking for  / followed by : no whitespaces
	std::size_t p0 = _rawBuffer.find("/");
	std::size_t p1 = _rawBuffer.find(":", p0 );
	if ( p0 >= 0 && p1 > p0 ){
		uri = _rawBuffer.substr( p0, p1-p0 );
		std::size_t p1 = uri.find("/");
		if ( p1 == 0 ){
			uri = uri.erase(0,1);
		}
	}
	return( uri );
}

/**
 * /venusOpcPilotServer/Device/src/DGENERICPILOT.cpp:22:10: error: 'boost/foreach.hpp' file not found [clang-diagnostic-error]
 * just everything from the end until the last : , we match backwards
 * for json/sarif, extra rules apply:
 * * no '
 * * no linefeeds
 */
std::string CTR::get_messageText(){
	std::string msg;
	unsigned int max = 512;
	std::size_t p0 = _rawBuffer.find_last_of(":");
	if ( p0 > 0 ){
		msg = _rawBuffer.substr( p0 + 1 , max );

		// need to replace " in the string, it confuses sarif
		std::size_t pos = msg.find("\"", 0);
		while ( pos != std::string::npos ){
			msg.replace( pos, 1, "<doublequote>");
			if ( pos + 1 <= msg.size() ) {
				pos = msg.find("\"", pos + 1);
			} else {
				break;
			}
		}
		// need to replace ' in the string, it confuses sarif
		pos = msg.find('\'', 0);
		while ( pos != std::string::npos ){
			msg.replace( pos, 1, "<singlequote>");
			if ( pos + 1 <= msg.size() ) {
				pos = msg.find('\'', pos + 1);
			} else {
				break;
			}
		}
		// need to replace return in the string, it confuses json
		pos = msg.find('\n', 0);
		while ( pos != std::string::npos ){
			msg.replace( pos, 1, "<return>");
			if ( pos + 1 <= msg.size() ) {
				pos = msg.find('\n', pos + 1);
			} else {
				break;
			}
		}

		// need to replace linefeed in the string, it confuses json
		pos = msg.find('\r', 0);
		while ( pos != std::string::npos ){
			msg.replace( pos, 1, "<return>");
			if ( pos + 1 <= msg.size() ) {
				pos = msg.find('\r', pos + 1);
			} else {
				break;
			}
		}
	}
	return( msg );
}


/**
 * just hardcode to avoid direct dependency on clang
 */
std::string CTR::get_toolInformationUri(){
	return("https://clang.llvm.org/extra/index.html");
}
/**
 * just hardcode to avoid direct dependency on clang
 */
std::string CTR::get_toolName(){
	return("clang-tidy");
}
/**
 * just hardcode to avoid direct dependency on clang
 */
std::string CTR::get_toolVersion(){
	return("14.0.0 Ubuntu LLVM version");
}


