/**
# LICENSE:
# Copyright (c) 2023, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Author: Michael.Ludwig@cern.ch
*/
/**
 * main.cpp
 */

#include <main.h>
#include <../build/generated/VERSION.h>

void usage( void ){

	std::cout << "clang-tidy-sarif: [-h, --help] this help" << std::endl;
	std::cout << "clang-tidy-sarif: digest (multiple) reports (from ./) and pack them into one SARIF (json) with multiple runs" << std::endl;
	std::cout << "clang-tidy-sarif [<-i input dir>, <-o output file>] " << std::endl;
	std::cout << "	defaults: input dir=./ output file=./output.sarif " << std::endl;
	exit(0);
}


int main(int argc, char** argv){

	std::cout << __FILE__ << " " << __LINE__ << " version= " << CLANG_TIDY_SARIF_VERSION << " " << __DATE__ << " " << __TIME__ << std::endl;

	if ( argc < 2 ) usage();
	for ( int i = 1; i < argc ; i++ ){
		if ( std::string::npos != std::string(argv[i]).find("--help") ) usage();
		if ( std::string::npos != std::string(argv[i]).find("-h") ) usage();
	}
	std::string idirname = ".";
	std::string ofilename = "output.sarif";
	for ( int i = 1; i < argc - 1 ; i++ ){
		if ( 0 == strcmp( argv[i], "-i") ) {
			char xx[256];
			sscanf( argv[ i + 1 ], "%s", (char *) &xx );
			idirname = std::string(xx);
		   	std::cout << " running with input dir= " << idirname << std::endl;
		}
		if ( 0 == strcmp( argv[i], "-o") ) {
			char xx[256];
			sscanf( argv[ i + 1 ], "%s", (char *) &xx );
			ofilename = std::string(xx);
		   	std::cout << " writing output to filename= " << ofilename << std::endl;
		}
	}


	sar_ns::SAR* sar = new sar_ns::SAR();
	sar->add_run();
	sar_ns::SAR::RUN_t *run = sar->get_run( sar->get_index_run() ); // current run

	/**
	 * now we add the results to that run.
	 * fundamentally, we extract info from the ctr and copy it to the sarif structure.
	 */
    for ( auto& file : std::filesystem::directory_iterator{ idirname } )  //loop through the current folder
    {
    	//std::cout << file.path() << std::endl;
    	std::string fn = std::string(file.path());
    	if ( fn.find( ".ctr" ) != std::string::npos) {
        	std::cout << " reading CTR from file: " << fn << std::endl;

        	ctr_ns::CTR* ctr = new ctr_ns::CTR( fn );
        	float size = ctr->read();
        	if ( (float) size / 1024.0 > 1.0 ){
        		std::cout << "read " << size/1024 << " kBytes from " << fn << std::endl;
        	} else {
        		std::cout << "read " << size << " Bytes from " << fn << std::endl;
        	}

        	/**
        	 * we can have several issues and locations in one file!
        	 * we can use the physicalLocation in the CTR as delimiter between the issues: {artifactLocationUri, line, column}
        	 * loop through all issues/locations: check if there is an issue left
        	 */
        	unsigned int issue_line = ctr->get_regionLine();
        	unsigned int issue_column = ctr->get_regionColumn();
        	std::string issue_uri = ctr->get_artifactLocationUri();
             	while ( issue_line > 0 && issue_column > 0 ){

            	sar->add_result( run );
               	sar_ns::SAR::RESULT_t *result = sar->get_result( run, sar->get_index_result() );
               	sar->add_location( result );

            	// one result can have several locations? no.. just several "types of locations"
            	sar_ns::SAR::LOCATION_t *location = sar->get_location(result, 0 );

        		sar->set_ruleId( result, ctr->get_rule() );
        		sar->set_level( result, ctr->get_level() );
        		sar->set_messageText( result, ctr->get_messageText() );

          		sar->set_regionLine( location, issue_line );
           		sar->set_regionColumn( location, issue_column );
           		sar->set_artifactLocationUri( location, issue_uri );

        		// still inside the run, we set the tool after the result
        		sar->set_toolInformationUri( run, ctr->get_toolInformationUri() );
        		sar->set_toolName( run, ctr->get_toolName() );
        		sar->set_toolVersion( run, ctr->get_toolVersion() );

        		// we have digested this result/issue, remove it from the ctr buffer
        		ctr->removeFirstIssue();

        		// is there a next result/issue ?
            	issue_line = ctr->get_regionLine();
            	issue_column = ctr->get_regionColumn();
        	}
    	}
    }
    std::cout << "got " << sar->getGlobalResultCounter() << " results in total" << std::endl;
    sar->serialize2json( ofilename );
    std::cout << " written to " << ofilename << std::endl;
	return( 0 );
}
