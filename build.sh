#!/bin/bash
cmake -S . -B ./build -DCMAKE_EXPORT_COMPILE_COMMANDS=1 .
cd ./build && make -j 5
find ./ -name "compile_commands.json" -exec cp -v {} ../ \;
cd ..
