#!/bin/bash
# cleanup stale files after cmake
#make clean
find ./ -name "CMakeCache.txt" -exec rm -vf {} \;
find ./ -name "Makefile" -exec rm -vf {} \;
find ./ -name "cmake_install.cmake" -exec rm -vf {} \;
find ./ -type d -name "CMakeFiles" -exec rm -rvf {} \;

# toplevel artifacts
find ./ -type d -name "bin" -exec rm -rvf {} \;
find ./ -type d -name "lib" -exec rm -rvf {} \;
find ./ -type d -name "build" -exec rm -rvf {} \;

# refresh all VERSION.h
find ./ -type d -name "VERSION.h" -exec rm -vf {} \;

# cleanup compilation database
# keep the last one
#find ./ -name "compile_commands.json" -exec rm -f {} \;
