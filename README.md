# clang-tidy-sarif

this terminal utility (written in C++) takes raw clang-tidy issue reports (as spit out into stdout) and aggregates them into multiple runs into one sarif 2.1.0 report. I try to capture all information available from the reports and add any missing bits (using options). The resulting sarif can be validated at https://sarifweb.azurewebsites.net/validation . This sarif can then be fed into sonarqube9.9.LTS vi the external scanner. 

command line input: options, directory, output file name
input: a directory containing many files which are stdout pipes from clang-tidy. I give them the extension *.ctr (for C-lang T-idy R-eport).
output: one sarif file containing all identified issues

## version 1.0.0
- output validates on both
   - https://samate.nist.gov/SARD/sarif-validator
   - https://sarifweb.azurewebsites.net/Validation
   and is now hopefully digested by sonarqube
- improved file reading
- cleaned up message text for full json compatibility, it is very picky

## version 0.1
- works correctly for one result per run

